import { showModal } from "./modal";
import { createElement } from '../helpers/domHelper';

function createWinnerDetails(fighter) {
  // show winner name and image
  const { name, source } = fighter;
  const attributes = { src: source };

  var fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  var fighterName = createElement({ tagName: 'span', className: 'fighter-name' });
  const fighterImage = createElement({ tagName: 'img', className: 'fighter-image', attributes });


  fighterName.innerText = 'Name: ' + name;

  fighterDetails.append(fighterName);
  fighterDetails.append(fighterImage);


  return fighterDetails;

}

export function showWinnerModal(fighter) {
  const title = 'Winner info';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}