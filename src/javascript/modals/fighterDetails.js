import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';


export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;
  const attributes = { src: source };

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' })
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' })
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' })
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes })

  // show fighter name, attack, defense, health, image

  nameElement.innerText = 'Name:' + name + "\n";
  attackElement.innerText = 'Attack: ' + attack + "\n";
  defenseElement.innerText = 'Defense: ' + defense + "\n";
  healthElement.innerText = 'Health: ' + health;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
