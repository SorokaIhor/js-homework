

export function fight(firstFighter, secondFighter) {
  // return winner

  while (firstFighter.health > 0 && secondFighter.health > 0) {

    var firstFighterDamage = getDamage(firstFighter, secondFighter);
    var secondFighterDamage = getDamage(secondFighter, firstFighter);


    console.log(firstFighter.name + ' hits ' + secondFighter.name + ' for ' + firstFighterDamage);
    applyDamage(secondFighter, firstFighterDamage);
    console.log(secondFighter.name + "'s hp is " + secondFighter.health);
    if (secondFighter.health <= 0) {
      return firstFighter;
    }

    console.log(secondFighter.name + ' hits ' + firstFighter.name + ' for ' + secondFighterDamage);
    applyDamage(firstFighter, secondFighterDamage);
    console.log(firstFighter.name + "'s hp is " + firstFighter.health);
    if (firstFighter.health <= 0) {
      return secondFighter;
    }


  }

}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage 
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0
    ? damage
    : 0;
}

export function getHitPower(fighter) {
  // return hit power
  var criticalHitChance = (Math.floor(Math.random() * 2) + 1);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  var dodgeChance = (Math.floor(Math.random() * 2) + 1);
  return fighter.defense * dodgeChance;
}

function applyDamage(fighter, damage) {
  fighter.health -= damage;
}